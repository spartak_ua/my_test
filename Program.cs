﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lesson_9_task_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите размер массива: ");
            int[] testArray = new int[Convert.ToInt32(Console.ReadLine())];

            ArrayFill(testArray);
                        
            // 1. Maximum Value Сделал двумя вариантами. встроиной функцией и написаной своей
            Console.WriteLine("\n1st variant Maximum Value is: "+ testArray.Max());
            Console.WriteLine("2nd variant Maximum Value is: " + MaxValue(testArray));
            // 2. Minimum Value Сделал двумя вариантами. встроиной функцией и написаной своей
            Console.WriteLine("\n1st variant Minimum Value is: " + testArray.Min());
            Console.WriteLine("2nd variant Minimum Value is: " + MinValue(testArray));
            // 3. Sum of all elements Сделал двумя вариантами. встроиной функцией и написаной своей
            Console.WriteLine("\n1st variant Summ of all elements is: " + testArray.Sum());
            Console.WriteLine("2nd variant Summ of all elements is: " + SumValue(testArray));
            // 4. Arismetical mean of all elements Сделал двумя вариантами. встроиной функцией и написаной своей
            Console.WriteLine("\n1st variant Arithmetical mean of all elements is: " + testArray.Average());
            Console.WriteLine("2nd variant Arithmetical mean of all elements is: " + MeanValue(testArray));

            // 5. All odd values
            OddValue(testArray);

            Console.ReadLine();
        }
        //Метод для генерирования элементов массива и вывод его на экран
        static void ArrayFill(int[] array)
        {
            Random rand = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(100) ;

                if (i == 0) Console.Write("Сгенерированный массив: " + array[i] + " ");
                else Console.Write(array[i] +" ");
            }                
        }
        static void OddValue(int[] array) 
        {
            Console.Write("\nOdds Values: ");
            for (int i = 0; i < array.Length; i++) 
            {
                if (array[i] % 2 == 1) Console.Write(array[i] + " ");
            }
        }
        static int MaxValue(int[] array) 
        {
            int maxValue = array[0];
            for (int i = 1; i < array.Length; i++) 
            {
                if (maxValue < array[i]) maxValue = array[i];                      
            }
            return maxValue;
        }
        static int MinValue(int[] array)
        {
            int minValue = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                if (minValue > array[i]) minValue = array[i];
            }
            return minValue;
        }
        static int SumValue(int[] array)
        {
            int sumValue = default;
            for (int i = 0; i < array.Length; i++)
            {
                 sumValue += array[i];
            }
            return sumValue;
        }
        static double MeanValue(int[] array)
        {
            double meanValue = default;
            for (int i = 0; i < array.Length; i++)
            {
                meanValue += array[i];
            }
            meanValue /= array.Length;
            return meanValue;
        }
    }
}
